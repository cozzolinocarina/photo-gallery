import * as functions from "firebase-functions";
import * as admin from 'firebase-admin';

import { Foto } from "./photos";


export const crearRegistroDeFoto = functions.storage
    .bucket()
    .object()
    .onFinalize(async(obj:functions.storage.ObjectMetadata) => {
        //Verifico si es una foto, y sino salimos
        if(!obj.contentType || obj.contentType != 'image/jpeg' || !obj.name)
            return;

        const store = admin.firestore();
        const collection = store.collection('fotos');
        let foto:Foto={
            ruta: obj.name,
            rutaEnLaVista: '',
        };
        //Agrego la nueva foto a la colección
        await collection.add(foto);

    } 
    )

    functions.https.onCall((datos) => {
        return true;
    })
    functions.https.onRequest((ped,resp)=> {

    });