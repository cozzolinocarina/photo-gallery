import { Component, OnInit } from '@angular/core';
import { PhotoService } from '../services/photo.service';

import { ActionSheetController } from '@ionic/angular';
import { Foto } from '../models/photos';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  constructor(public fotosServ: PhotoService,
    public actionSheetController: ActionSheetController) { }

  async ngOnInit() {
    await this.fotosServ.cargarFotos();
  }
  
  agregarFotoALaGaleria() {
    this.fotosServ.agregarAGaleria();
  }
  public async MostrarAcciones(foto: Foto, posicion: number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Fotos',
      buttons: [{
        text: 'Borrar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.fotosServ.borrarFoto(foto, posicion);
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
          }
      }]
    });
    await actionSheet.present();
  }
}
