import { Injectable, OnDestroy } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Storage } from '@capacitor/storage';
import { Foto } from '../models/photos';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { Platform } from '@ionic/angular';
import { getFunctions, httpsCallable} from 'firebase/functions';
import { docSnapshots } from '@angular/fire/firestore';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Injectable({
  providedIn: 'root'
})
export class PhotoService implements OnDestroy{
  private fotos: Foto[] = [];
  private PHOTO_STORAGE: string = "fotos";

  constructor(private firestore: AngularFirestore,
    private storage: AngularFireStorage,
    private functions: AngularFireFunctions,
    private platform: Platform,
    ) 
    {
      let funcion = httpsCallable(getFunctions(),'listaFotos');
      funcion({})
      fetch('https:///listados');  
    
   }
  private async readAsBase64(cameraPhoto: Photo) {
    // Convierte la foto de blob a Base64
    /*
    const response = await fetch(cameraPhoto.webPath!);
    const blob = await response.blob();
  
    return await this.convertBlobToBase64(blob) as string;
    */
    // "hybrid" will detect Cordova or Capacitor
    if (this.platform.is('hybrid')) {
      // Read the file into base64 format
      const file = await Filesystem.readFile({
        path: cameraPhoto.path
      });
  
      return file.data;
    }
    else {
      //Lee plataforma web
      // Fetch the photo, read as a blob, then convert to base64 format
      const response = await fetch(cameraPhoto.webPath);
      const blob = await response.blob();
  
      return await this.convertBlobToBase64(blob) as string;
      }
    
  }
  
  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });


  private async guardarFoto(cameraPhoto: Photo):Promise<Foto> {

    // Convert photo to base64 format, required by Filesystem API to save
    const base64Data = await this.readAsBase64(cameraPhoto);

    // Write the file to the data directory
    const fileName = new Date().getTime() + '.jpeg';
    try{
      await this.storage.ref(fileName).putString(base64Data,'data_url',{contentType:'image/jpeg'});
    }catch(error){
      console.log(error);
    }
/*
 // Convert photo to base64 format, required by Filesystem API to save
    const base64Data = await this.readAsBase64(cameraPhoto);

    // Write the file to the data directory
    const fileName = new Date().getTime() + '.jpeg';
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data
    });

    if (this.platform.is('hybrid')) {
      // Display the new image by rewriting the 'file://' path to HTTP
      // Details: https://ionicframework.com/docs/building/webview#file-protocol
      return {
        filepath: savedFile.uri,
        webviewPath: Capacitor.convertFileSrc(savedFile.uri),
      };
    }
    else {
      // Use webPath to display the new image instead of base64 since it's
      // already loaded into memory
      return {
        filepath: fileName,
        webviewPath: cameraPhoto.webPath
      };
    }
*/
  // Use webPath to display the new image instead of base64 since it's
  // already loaded into memory
    return {
      ruta: fileName,
      rutaEnLaVista: cameraPhoto.webPath
    };

  }

  public async agregarAGaleria() {
    // Tomar una fotografía
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });

      // Save the picture and add it to photo collection
    const savedImageFile = await this.guardarFoto(capturedPhoto);
  
    /*const collection  = this.firestore.collection<Foto>('fotos');
    let docd = await collection.add(savedImageFile);
    savedImageFile.id = docd.id;
    */
   const funcion = this.functions.httpsCallable('listadoFotos');
   funcion({})
    this.fotos.unshift(savedImageFile);
    /*
    Storage.set({
      key: this.PHOTO_STORAGE,
      value: JSON.stringify(this.fotos)
    });*/  
  }
  private suscritptores:{
    lista:Foto[],
    unsub:any,
    subs:Subscriber<Foto[]>[],
  } = {
    lista:[],
    unsub:null,
    subs:[],
  }
  public listaDeFotos():Observable<Foto[]>
  { 
    return new Observable ((suscriptor:Subscriber <Foto[]>) => {
      //devuelve la nueva longitud, la posición es uno menos
      let subIndice = this.suscritptores.subs.push(suscriptor) - 1;
      if(this.suscritptores.lista.length)
        suscriptor.next(this.suscritptores.lista);
      if(!this.suscritptores.unsub){
        const collection  = this.firestore.collection<Foto>('fotos');
        this.suscritptores.unsub =collection.get().subscribe({
          error: (err) => {
            this.suscritptores.subs.forEach(sub => sub.error(err));
          },
          complete: () => {
            this.suscritptores.subs.forEach( sub => sub.complete());
          },
          next: (docs) => {
            this.fotos = [];
          for(let doc of docs.docs){
            let foto:Foto = doc.data();
            foto.id = doc.id;
            this.storage.ref(foto.ruta).getDownloadURL().toPromise().then( url => {
              foto.rutaEnLaVista = url;
              this.fotos.push(foto);
              //cuando tengo todas las URLs ya terminé
              if(this.fotos.length == docs.docs.length){
                this.suscritptores.lista = this.fotos;
                this.suscritptores.subs.forEach(sub => sub.next(this.suscritptores.lista));
              };
            });
          };
        }
      });
    } 
    return {unsubscribe:() => {
      this.suscritptores.subs.splice(subIndice,1);
      if(!this.suscritptores.subs.length){
          this.suscritptores.unsub.unsubscribe();
          this.suscritptores.unsub = null;
      };
    }};
    });

  }
  

  private subs:Subscription[] = [];

  public cargarFotos() {

}

ngOnDestroy(){
  this.subs.forEach( sub => sub.unsubscribe());
}
  public async borrarFoto(foto: Foto, posicion: number) {
    // Quitar la foto del arreglo de fotos
    this.fotos.splice(posicion, 1);
  
    // Actualizar el arreglo de fotos, en el almacenamiento
    const collection = this.firestore.collection<Foto>('fotos');
    await collection.doc(foto.id).delete();

    //borrar el archivo físico en storage
    const fileName = foto.ruta.substr(foto.ruta.lastIndexOf('/')+1);
    try{
      await this.storage.ref(fileName).delete().toPromise;
    }catch(error){
      console.log(error);
    }

    /*Storage.set({
      key: this.PHOTO_STORAGE,
      value: JSON.stringify(this.fotos)
    });
  
    // Borrar la foto del sistema de archivos
    const filename = foto.ruta
                        .substr(foto.ruta.lastIndexOf('/') + 1 ); 
  
   try
   {
      await Filesystem.deleteFile({
      path: filename,
      directory: Directory.Data
    });
  }
  catch(error){
      console.log(error);
    }*/
  }
}