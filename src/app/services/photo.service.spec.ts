import { TestBed } from '@angular/core/testing';

import { PhotoService } from './photo.service';

describe('PhotoService', () => {
  let service: PhotoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PhotoService);
  });

  it('debería crearse', () => {
    expect(service).toBeTruthy('Pero no ha sido creado');
  });

  it('la lista de fotos es un arreglo válido',()=> {
    expect(service.listaDeFotos()).toBeTruthy('realmente devuelve algo');
    expect(service.listaDeFotos().length).toBeGreaterThanOrEqual(0,'pero no tiene longitud, por lo que no es un arreglo válido');
  })

  fit('carga correctamente las fotos almacenadas', (done) => {
    service.cargarFotos().then(() => {
     expect(service.listaDeFotos().length).toBe(0,'uhhmmm');
     done();
  })
},20000);
});
