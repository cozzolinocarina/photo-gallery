// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'inv-photogallery-f9c10',
    appId: '1:1063555089473:web:be876a2e35897b9ec072c0',
    databaseURL: 'https://inv-photogallery-f9c10-default-rtdb.firebaseio.com',
    storageBucket: 'inv-photogallery-f9c10.appspot.com',
    locationId: 'southamerica-east1',
    apiKey: 'AIzaSyCHmYB36aurFg3SlSniaPnzQiE7MOg70tA',
    authDomain: 'inv-photogallery-f9c10.firebaseapp.com',
    messagingSenderId: '1063555089473',
    measurementId: 'G-VPDRV56B5Q',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
