export const environment = {
  firebase: {
    projectId: 'inv-photogallery-f9c10',
    appId: '1:1063555089473:web:be876a2e35897b9ec072c0',
    databaseURL: 'https://inv-photogallery-f9c10-default-rtdb.firebaseio.com',
    storageBucket: 'inv-photogallery-f9c10.appspot.com',
    locationId: 'southamerica-east1',
    apiKey: 'AIzaSyCHmYB36aurFg3SlSniaPnzQiE7MOg70tA',
    authDomain: 'inv-photogallery-f9c10.firebaseapp.com',
    messagingSenderId: '1063555089473',
    measurementId: 'G-VPDRV56B5Q',
  },
  production: true
};
